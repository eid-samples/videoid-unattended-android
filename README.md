# Android VideoID Demo

This is a demo application which shows how to use of the Android SDK to record an **Unattended VideoID**. It contains a simple Activity which starts a default VideoIDActivity.

## How to run the demo

The only requirement to run the application is to complete the environment configuration:
  - URL: The URL where VideoID service is running
  - Bearer: The authorization token to access the service

And set the idType of the document to be scanned (e.g. 62 for Spanish ID card).

```java
    intent.putExtra(VideoIDActivity.INTENT_ENVIRONMENT, new Environment("videoid_service_ip", "my_bearer"));
    intent.putExtra(VideoIDActivity.INTENT_LANGUAGE, Language.EN);
    intent.putExtra(VideoIDActivity.INTENT_DOCUMENT_TYPE, 62);
```